#include <stdlib.h>
#define ARR_SIZE 50
#define TREE_SIZE 100

#define true 1 == 1
#define false 1 != 1
typedef int boolean;

typedef struct stBranch
{
	int m_nKey;
	stBranch* m_pstLeft;
	stBranch* m_pstRight;
}stBranch;

typedef struct stStackNode
{
	stBranch* m_pstData;
	stStackNode* m_pstNext;
}stStackNode;

typedef struct stStack
{
	stStackNode* m_pstHead;
	int m_nCount;
}stStack;

void push(stStack* pstStack, stBranch* pstPushed)
{
	stStackNode* pstTemp = (stStackNode*)malloc(sizeof(stStackNode));
	pstTemp->m_pstData = pstPushed;
	pstTemp->m_pstNext = pstStack->m_pstHead;
	pstStack->m_pstHead = pstTemp;
	pstStack->m_nCount++;
}

stBranch* pop(stStack* pstStack)
{
	if (pstStack->m_nCount <= 0)
		return NULL;

	stStackNode* pstTemp;

	pstTemp = pstStack->m_pstHead;
	pstStack->m_pstHead = pstTemp->m_pstNext;
	pstStack->m_nCount--;

	stBranch* pstPoped = pstTemp->m_pstData;
	free(pstTemp);

	return pstPoped;
}

stBranch* treeInsert(stBranch* t, int data) 
{
	stBranch* newBranch;
	newBranch = (stBranch*)malloc(sizeof(stBranch));
	newBranch->m_nKey = data;
	newBranch->m_pstLeft = NULL;
	newBranch->m_pstRight = NULL;
	stBranch* pstCurrent = t;
	stBranch* pstParent = t;

	if (t == NULL) 
		t = newBranch;
	else 
	{
		while (pstCurrent->m_nKey != data) 
		{
			pstParent = pstCurrent;

			if (pstCurrent->m_nKey > data) {
				pstCurrent = pstCurrent->m_pstLeft;
				if (pstCurrent == NULL) 
				{
					pstParent->m_pstLeft = newBranch;
					return t;
				}
			}
			else 
			{
				pstCurrent = pstCurrent->m_pstRight;
				if (pstCurrent == NULL) 
				{
					pstParent->m_pstRight = newBranch;
					return t;
				}
			}
		}
	}

	return t;
}

void printBinTree(stBranch* root) 
{
	if (root) 
	{
		printf("%d", root->m_nKey);

		if (root->m_pstLeft || root->m_pstRight) 
		{
			printf("(");

			if (root->m_pstLeft)
				printBinTree(root->m_pstLeft);
			else
				printf("NULL");

			printf(",");

			if (root->m_pstRight)
				printBinTree(root->m_pstRight);
			else
				printf("NULL");

			printf(")");
		}
	}
}

stBranch* getSuccessor(stBranch* node) 
{
	stBranch* current = node->m_pstRight;
	stBranch* parent = node;
	stBranch* s = node;

	while (current != NULL) 
	{
		parent = s;
		s = current;
		current = current->m_pstLeft;
	}

	if (s != node->m_pstRight) 
	{
		parent->m_pstLeft = s->m_pstRight;
		s->m_pstRight = node->m_pstRight;
	}

	return s;
}

boolean treeNodeDelete(stBranch* root, int key) 
{
	stBranch* current = root;
	stBranch* parent = root;
	boolean isLeftChild = true;

	while (current->m_nKey != key) 
	{
		parent = current;

		if (key < current->m_nKey) 
		{
			current = current->m_pstLeft;
			isLeftChild = true;
		}
		else 
		{
			current = current->m_pstRight;
			isLeftChild = false;
		}

		if (current == NULL)
			return false;
	}

	if (current->m_pstLeft == NULL && current->m_pstRight == NULL) 
	{
		if (current == root)
			root = NULL;
		else if (isLeftChild)
			parent->m_pstLeft = NULL;
		else
			parent->m_pstRight = NULL;
	}
	else if (current->m_pstRight == NULL) 
	{
		if (isLeftChild)
			parent->m_pstLeft = current->m_pstLeft;
		else
			parent->m_pstRight = current->m_pstLeft;
	}
	else if (current->m_pstLeft == NULL) 
	{
		if (isLeftChild)
			parent->m_pstLeft = current->m_pstRight;
		else
			parent->m_pstRight = current->m_pstRight;
	}
	else 
	{
		stBranch* successor = getSuccessor(current);

		if (current == root)
			root = successor;
		else if (isLeftChild)
			parent->m_pstLeft = successor;
		else
			parent->m_pstRight = successor;
		successor->m_pstLeft = current->m_pstLeft;
	}

	return true;
}

int nCountLevels(stBranch* pstBranch, boolean blIsMax)
{
	int nLeft = 0, nRight = 0;

	if (pstBranch->m_pstLeft)
		nLeft = nCountLevels(pstBranch->m_pstLeft, blIsMax);

	if (pstBranch->m_pstRight)
		nRight = nCountLevels(pstBranch->m_pstRight, blIsMax);

	if (nLeft == nRight)
		return nLeft + 1;
	else if (((nLeft > nRight) && (blIsMax == true))
				||((nLeft < nRight) && (blIsMax == false)))
			return nLeft + 1;
	else
			return nRight + 1;
}

boolean blIsBalanced(stBranch* pstBranch)
{
	int nMin = 0, nMax = 0;

	nMin = nCountLevels(pstBranch, false);
	nMax = nCountLevels(pstBranch, true);

	if (nMax - nMin < 2)
		return true;
	else
		return false;
}

int nGetPercentage(stBranch** pstFirst, int nCount)
{
	int nBalanced = 0;

	for (int i = 0; i < nCount; i++)
	{
		if (blIsBalanced(pstFirst[i]) == true)
			nBalanced++;
	}

	return nBalanced * 100 / nCount;
}

boolean blIsFound(stBranch* pstBranch, int nFindData, stStack* pstStack)
{
	if (pstBranch == NULL)
		return false;

	push(pstStack, pstBranch);

	if (pstBranch->m_nKey == nFindData)
		return true;
	else if (pstBranch->m_nKey > nFindData)
		return blIsFound(pstBranch->m_pstLeft, nFindData, pstStack);
	else
		return blIsFound(pstBranch->m_pstRight, nFindData, pstStack);
}

int main()
{
	stBranch* arr_stTree[ARR_SIZE] = NULL;

	for (int i = 0; i < ARR_SIZE; i++)
	{
		for (int j = 0; j < TREE_SIZE; j++)
		{
			int nTemp = rand() % 1000;
			arr_stTree[i] = treeInsert(arr_stTree[i], nTemp);
		}
	}

	int nPercent = nGetPercentage(&arr_stTree, ARR_SIZE);

	printf("%d\n", nPercent);

	return 0;
}